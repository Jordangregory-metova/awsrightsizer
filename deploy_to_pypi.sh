#!/bin/bash -e
cat << EOF >> ~/.pypirc
[distutils]
index-servers=pypi
[pypi]
repository=https://upload.pypi.org/pypi
username=${pypi_username}
password=${pypi_password}
EOF

python setup.py sdist bdist_wheel upload;
rm ~/.pypirc